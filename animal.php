<?php

class Animal 
{
    public $cold_blooded = 'no';
    public $legs = 4;
    
    public $name;

    public function __construct($name)
    {
        $this->name = $name;
    }
}
