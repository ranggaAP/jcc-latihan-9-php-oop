<?php

class Ape 
{
    public $cold_blooded = 'no';
    public $legs = 2;
    public $yell = 'Auooo';
    
    public $name;

    public function __construct($name)
    {
        $this->name = $name;
    }
}
