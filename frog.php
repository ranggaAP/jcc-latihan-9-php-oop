<?php

class Frog 
{
    public $cold_blooded = 'no';
    public $legs = 4;
    public $jump = 'hop hop';
    
    public $name;

    public function __construct($name)
    {
        $this->name = $name;
    }
}
