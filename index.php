<?php

require('./animal.php');
require('./ape.php');
require('./frog.php');


$sheep      = new Animal("shaun");
$sungokong  = new Ape("kera sakti");
$frog       = new Frog("buduk");

echo 'Name : '.$sheep->name.'<br>';
echo 'legs : '.$sheep->legs.'<br>';
echo 'cold blooded : '.$sheep->cold_blooded.'<br><br>';

echo 'Name : '.$frog->name.'<br>';
echo 'legs : '.$frog->legs.'<br>';
echo 'cold blooded : '.$frog->cold_blooded.'<br>';
echo 'Jump : '.$frog->jump.'<br><br>';

echo 'Name : '.$sungokong->name.'<br>';
echo 'legs : '.$sungokong->legs.'<br>';
echo 'cold blooded : '.$sungokong->cold_blooded.'<br>';
echo 'Yell : '.$sungokong->yell.'<br><br>';
